

| Speaker                | Theme                                                             |
|:----------------------:|:------------------------------------------------------------------|
| Pierre-Henri Wuillemin (LIP6) | pyAgrum: Introduction, introspection and illustration             |
| Jeremy Chichportich (Bramham Gardens)    | Optimal Quantized Belief Propagation                              |
| Clara Charon (Teranga Software)           | Classification with pyAgrum for prediction in nursing homes       |
| Mahdi Hadj Ali (SAP)         | A Quantitative Explanation for pyAgrum Classifier: Shapley values |
| Christophe Gonzales (LIS)    | Fast and furious things in aGrUM/pyAgrum                                              |
| Marvin Lasserre (LIP6)        | Coupling aGrUM/pyAgrum with external libraries : an application to continuous non-parametric Bayesian Networks |
| Mélanie Munch (I2M)          | A process reverse engineering approach using Expert Knowledge and Probabilistic Relational Models |
| Santiago Cortijo (Scalnyx)       | Simpson's Paradox analyzed through Causal Reasoning                                   |
| Ketemwabi Yves Shamavu (University of Nebraska) | Beyond Black-box Models in Sensitive Environments                                     |
| Questions and Answers  |   
