from typing import NewType, Union

NodeId = NewType("NodeId", int)
NameOrId = Union[str, NodeId]
