"""
Class Ctbn : Continous Time Bayesian Network
"""
from typing import Dict, Tuple, List, Set

import pydotplus as dot
from IPython.display import display, SVG

import pyAgrum as gum

from .utils import NodeId, NameOrId
from .Cim import CIM


class Ctbn:
    graph: gum.DiGraph
    cim: Dict[NodeId, CIM]

    id2var: Dict[NodeId, gum.DiscreteVariable]
    name2id: Dict[str, NodeId]

    bn0: gum.BayesNet

    def _nameOrId(self, val: NameOrId) -> NodeId:
        return val if isinstance(val, int) else self.name2id[val]

    def __init__(self):
        self.graph = gum.DiGraph()
        self.bn0 = gum.BayesNet()
        self.cim = {}

        self.id2var = {}
        self.name2id = {}

    def add(self, var: gum.DiscreteVariable) -> NodeId:
        """
        Add a new variable to the Ctbn

        :param var: pyAgrum.DiscreteVariable

        :raises NameError: if a variable with the same name already exists

        :return: the NodeId
        """
        if var is None:
            raise SyntaxError("The argument can not be None.")
        if var.name=="" or not CIM.is_parent(var):
            raise NameError(f"The name '{var.name()}' is not correct.")

        if var.name() in self.name2id:
            raise NameError(f"A variable with the same name ({var.name()}) already exists in this Ctbn.")

        n = NodeId(self.graph.addNode())
        self.id2var[n] = var
        self.name2id[var.name()] = n

        self.bn0.add(var)

        v_i = var.clone()
        v_i.setName(CIM.var_i(var.name()))

        v_j = var.clone()
        v_j.setName(CIM.var_j(var.name()))

        self.cim[n] = CIM().add(v_j).add(v_i)

        return n

    def _nameOrId(self, val: NameOrId) -> NodeId:
        return val if isinstance(val, int) else self.name2id[val]

    def addArc(self, val1: NameOrId, val2: NameOrId) -> Tuple[NodeId, NodeId]:
        """
        Add an arc val->val2
        :param val1: name or id of a node
        :param val2: name or id of a node
        :raises gum.NotFound if one the names is not Found in the Ctbn
        :return: the created arc : (node1,node2)
        """
        n1 = self._nameOrId(val1)
        n2 = self._nameOrId(val2)

        self.graph.addArc(n1, n2)

        self.cim[n2].add(self.id2var[n1])

        return (n1, n2)

    def name(self, node: NodeId) -> str:
        """
        :param node:
        :raises gum.NotFound if node is not Found in the Ctbn
        :return: the name from the NodeId
        """
        return self.id2var[node].name()

    def node(self, name: str) -> NodeId:
        """
        :param name:
        :raises gum.NotFound if name is not Found in the Ctbn
        :return: the NodeId from the name
        """
        return self.name2id[name]

    def variable(self, val: NameOrId) -> gum.DiscreteVariable:
        """
        :param val: a name or a NodeId
        :raises gum.NotFound if val is not Found in the Ctbn
        :return: the variable from the NodeId or from the name
        """
        return self.id2var[self._nameOrId(val)]

    def nodes(self) -> List[NodeId]:
        """
        :return: the list of NodeIds in the Ctbn
        """
        return list(self.id2var.keys())

    def names(self) -> List[str]:
        """
        return the list of variable names in the CTNB
        :return:
        """
        return list(self.name2id.keys())

    def arcs(self) -> Set[Tuple[NodeId, NodeId]]:
        """
        :return: the list of arcs as a list of couple of NodeIds
        """
        return self.graph.arcs()

    def parents(self, val: NameOrId) -> Set[NodeId]:
        """
        :return: the list of parent ids from the name or the id of a node
        """
        return self.graph.parents(self._nameOrId(val))

    def parent_names(self, val: NameOrId) -> List[str]:
        """
        :return: the list of parents names from the name or the id of a node
        """
        return [self.name(n) for n in self.parents(val)]

    def children(self, val: NameOrId) -> Set[NodeId]:
        """
        :return: the list of children ids from the name or the id of a node
        """
        return self.graph.children(self._nameOrId(val))

    def children_names(self, val: NameOrId) -> List[str]:
        """
        :return: the list of children names from the name or the id of a node
        """
        return [self.name(n) for n in self.children(val)]

    def CIM(self, val: NameOrId) -> CIM:
        """
        :return: the conditionnal intensity matrix for the node
        """
        return self.cim[self._nameOrId(val)]

    def completeInstantiation(self):
        res = gum.Instantiation()
        for nod in self.nodes():
            res.add(self.variable(nod))
        return res

    def fullInstantiation(self):
        res = gum.Instantiation()
        for nod in self.nodes():
            res.add(self.variable(nod))
            res.add(self.CIM(nod).variable(0)) #v_i
            res.add(self.CIM(nod).variable(1)) #v_j
        return res

    def toDot(self):
        """
        Create a display of the graph representating the CTBN.

        Parameters
        ----------
        None.

        Returns
        -------
        A display of the graph.

        """
        chaine = """
        digraph  "ctbn" {
        graph [bgcolor=transparent,label=""];
        node [style=filled fillcolor="#ffffaa"];

        """

        # Add the name of the var
        for nomVar in self.names():
            chaine += '  "' + str(nomVar) + '"' + ";\n"
        chaine += " "

        # Adding arcs
        for arc in list(self.graph.arcs()):
            num1 = arc[0]
            num2 = arc[1]
            nom1 = self.name(num1)
            nom2 = self.name(num2)
            chaine += '  "' + str(nom1) + '"->"' + str(nom2) + '"' + ";\n"

        chaine = chaine[:-1]
        chaine += "}"
        return chaine

    def _repr_html_(self):
        g = dot.graph_from_dot_data(self.toDot())
        g.set_bgcolor("transparent")
        for n in g.get_nodes():
            n.set_style("filled")
            n.set_fillcolor("#FAEEEE")

        return SVG(g.create_svg()).data
