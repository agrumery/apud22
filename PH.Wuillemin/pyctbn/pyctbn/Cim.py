from typing import Dict, Optional

import numpy as np
import pyAgrum as gum


class CIM:
    DELIMITER = "#"
    _pot: gum.Potential

    def __init__(self, pot=None):
        if pot is None:
            self._pot = gum.Potential()
        else:
            self._pot = gum.Potential(pot)
        self._recordVars()

    def add(self, v) -> "CIM":
        self._pot.add(v)
        self._recordVars()
        return self

    def nbrDim(self) -> int:
        return self._pot.nbrDim()

    def extract(self, ctxt) -> "CIM":
        return CIM(self._pot.extract(ctxt))

    @property
    def var_names(self):
        return self._pot.var_names

    def __getitem__(self, i):
        return self._pot[i]

    def __setitem__(self, i, v):
        self._pot[i] = v

    def variablesSequence(self):
        return self._pot.variablesSequence()

    def variable(self, arg):
        return self._pot.variable(arg)

    def instantiation(self):
        return gum.Instantiation(self._pot)

    def _recordVars(self):
        self._parents = set()
        self._bases = set()
        for v in self.variablesSequence():
            if self.is_parent(v):
                self._parents.add(v)
            else:
                self._bases.add(v)

    @staticmethod
    def var_i(name: str) -> str:
        return f"{name}{CIM.DELIMITER}i"

    @staticmethod
    def var_j(name: str) -> str:
        return f"{name}{CIM.DELIMITER}j"

    @staticmethod
    def var_radical(v: gum.DiscreteVariable) -> str:
        if CIM.is_parent(v):
            return v.name()
        return v.name()[:-2]

    @staticmethod
    def is_parent(v: gum.DiscreteVariable) -> bool:
        """
        use the syntax convention to check if a name is the name of a parent in a CIM

        :param name: the variable name
        :return: True if name corresponds to the name of a parent in a CIM.
        """
        name = v.name()
        if len(name) < 2:
            return True
        if name[-2] != CIM.DELIMITER:
            return True
        return False

    def is_IM(self) -> bool:
        """
        :return: True if there is no conditioning variable (parent) in the CIM
        """
        for v in self.variablesSequence():
            if self.is_parent(v):
                return False
        return True

    def to_matrix(self, ctxt: Optional[Dict[str, str]] = None) -> np.array:
        if ctxt is not None:
            q = self.extract(ctxt)
        else:
            q = self
        if not q.is_IM():
            raise ValueError("The cim is conditionnal.")
        i = q.instantiation()
        iI = gum.Instantiation()
        iJ = gum.Instantiation()
        for n in sorted(q.var_names):  # to be sure of a deterministic order
            v = i.variable(n)
            if n[-1] == "i":
                iI.add(v)
            else:
                iJ.add(v)

        res = []
        iI.setFirst()
        while not iI.end():
            iJ.setFirst()
            line = []
            while not iJ.end():
                i.setVals(iI)
                i.setVals(iJ)
                line.append(q[i])
                iJ.inc()
            res.append(list(line))
            iI.inc()

        return np.array(res)

    def from_matrix(self, mat):
        if not self.is_IM():
            raise ValueError("The cim is conditionnal.")
        i = self.instantiation()
        iI = gum.Instantiation()
        iJ = gum.Instantiation()
        siz = 1
        for n in sorted(self.var_names):  # to be sure of a deterministic order
            v = i.variable(n)
            if n[-1] == "i":
                iI.add(v)
                siz *= v.domainSize()
            else:
                iJ.add(v)
        if mat.shape != (siz, siz):
            raise AttributeError(f"Shape {mat.shape} should be {(siz, siz)}")

        iI.setFirst()
        for lin in mat:
            iJ.setFirst()
            for v in lin:
                i.setVals(iI)
                i.setVals(iJ)
                self[i] = v
                iJ.inc()
            iI.inc()

    def amalgamate(self: "CIM", cimY: "CIM") -> "CIM":
        cimX = self

        if cimX.nbrDim() == 0:
            return CIM(gum.Potential(cimY._pot))
        if cimY.nbrDim() == 0:
            return CIM(gum.Potential(cimX._pot))

        sX = {self.var_radical(v) for v in cimX.variablesSequence()
              if not self.is_parent(v)}
        sY = {self.var_radical(v) for v in cimY.variablesSequence()
              if not self.is_parent(v)}

        amal = CIM()
        for v in cimX._bases.union(cimY._bases):
            amal.add(v)

        pXinY = set()
        pYinX = set()
        for v in cimX._parents:
            if v.name() not in sY:
                amal.add(v)
            else:
                pYinX.add(v)

        for v in cimY._parents:
            if v.name() not in sX:
                amal.add(v)
            else:
                pXinY.add(v)

        amal._recordVars()

        i = amal.instantiation()
        iX = cimX.instantiation()
        iY = cimY.instantiation()
        i.setFirst()
        while not i.end():
            iX.setVals(i)
            iY.setVals(i)
            for v in pXinY:
                iY.chgVal(v, iX[CIM.var_i(v.name())])
            for v in pYinX:
                iX.chgVal(v, iY[CIM.var_i(v.name())])
            dX = True
            for v in sX:
                if iX[CIM.var_i(v)] != iX[CIM.var_j(v)]:
                    dX = False
                    break
            dY = True
            for v in sY:
                if iY[CIM.var_i(v)] != iY[CIM.var_j(v)]:
                    dY = False
                    break
            if dX and dY:
                amal[i] = cimX[iX] + cimY[iY]
            elif dY:
                amal[i] = cimX[iX]
            elif dX:
                amal[i] = cimY[iY]
            else:
                amal[i] = 0

            i.inc()

        return amal

    def __mul__(self: "CIM", cimY: "CIM") -> "CIM":
        return self.amalgamate(cimY)
