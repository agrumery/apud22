from typing import Dict, Tuple, Union, List, NewType, Set

from concurrent.futures import ProcessPoolExecutor

import numpy as np
from numpy.random import default_rng, choice

import pyAgrum as gum

from .Ctbn import Ctbn
from .Cim import CIM
from scipy.linalg import expm


class CtbnInference:
    def __init__(self, model: Ctbn):
        self._model = model

    def makeInference(self):
        raise NotImplemented("Not implemented yet.")

    def posterior(self, name: str) -> gum.Potential:
        raise NotImplemented("Not impletemented yet.")


class SimpleCtbnInference(CtbnInference):
    '''
    Exact inference using amalgamation to compute the Intensity Matrix corresponding to the ctbn
    (very bad for large models)
    '''

    def __init__(self, cim: Ctbn):
        super().__init__(cim)
        self._joint = None

    def makeInference(self):
        q = CIM()
        for nod in self._model.nodes():
            q = q.amalgamate(self._model.CIM(nod))

        q.from_matrix(expm(5000 * q.to_matrix()))

        t0 = gum.Potential()
        for n in q.var_names:
            if n[-1] == "i":
                t0.add(q._pot.variable(n))
        t0.fillWith(1).normalize()

        self._joint = (t0 * q._pot).margSumOut(t0.var_names)

    def posterior(self, name: str) -> gum.Potential:
        vj = CIM.var_j(name)
        return gum.Potential().add(self._model.variable(name)).fillWith(self._joint.margSumIn(vj), [vj])


class ForwardSampleCtbnInference(CtbnInference):
    '''
    Inference using forward sampling (slow convergence)
    '''

    def __init__(self, cim: Ctbn):
        super().__init__(cim)
        self._posteriors = {nod: gum.Potential().add(self._model.variable(nod))
                            for nod in self._model.names()}

    def makeSample(self, posteriors, timeHorizon: float = 5000, burnIn: int = 100):
        def init(current):
            for nam in self._model.names():
                posteriors[nam].fillWith(0)

            # initial uniform distribution for each variable
            for nod in self._model.nodes():
                v = self._model.variable(nod)
                current.chgVal(v.name(), int(choice(range(v.domainSize()))))

        def getNextEvent(current, indice):
            indice.setVals(current)
            # compute next variable event
            dt = argmin = None
            for nod in self._model.nodes():
                v = self._model.variable(nod)
                indice.chgVal(CIM.var_i(v.name()), indice.val(v))
                indice.chgVal(CIM.var_j(v.name()), indice.val(v))  # just the diagonal

                v_lambda = -self._model.CIM(nod)[indice]
                if v_lambda != 0 :
                    d = rand.exponential(1 / v_lambda)
                    if dt is None or dt > d:
                        dt = d
                        argmin = nod

            return argmin, dt

        def sampleNextState(current, indice, nextEvt):
            # compute the nextstate
            v = self._model.variable(nextEvt)
            indice.chgVal(CIM.var_i(v.name()), indice.val(v))
            indice.chgVal(CIM.var_j(v.name()), indice.val(v))  # just the diagonal

            somme = -self._model.CIM(nextEvt)[indice]
            choices = []
            probas = []
            for j in range(v.domainSize()):
                if j != indice.val(v):  # not the diagonal
                    choices.append(j)
                    indice.chgVal(CIM.var_j(v.name()), j)
                    probas.append(self._model.CIM(nextEvt)[indice] / somme)

            current.chgVal(v.name(), int(choice(choices, p=probas)))

        current = self._model.completeInstantiation()
        rand = default_rng()
        iteration = 0
        duration = 0
        indice = self._model.fullInstantiation()

        init(current)
        while True:
            nextEvt, dt = getNextEvent(current, indice)

            if duration + dt > timeHorizon:
                # update everyone for last event
                for nam in self._model.names():
                    posteriors[nam][current] += timeHorizon - duration
                break

            # updating stats (if not burnIn)
            if iteration > burnIn:
                for nam in self._model.names():
                    posteriors[nam][current] += dt
                duration += dt

            sampleNextState(current, indice, nextEvt)

            iteration += 1

        return iteration

    def makeInference(self, timeHorizon: float = 5000, burnIn: int = 100):
        res = self.makeSample(self._posteriors, timeHorizon, burnIn)
        for nam in self._model.names():
            self._posteriors[nam].normalize()
        return res

    def makeParallelInference(self, trajectories: int = 5, timeHorizon: float = 5000, burnIn: int = 100):
        posteriorsList = [{nod: gum.Potential().add(self._model.variable(nod))
                           for nod in self._model.names()} for i in range(trajectories)]

        def runMakeSample(task: int):
            res = self.makeSample(posteriorsList[task], timeHorizon, burnIn)
            return res

        with ProcessPoolExecutor(max_workers=1000) as executor:
            future = {executor.submit(runMakeSample, task) for task in range(trajectories)}

        print([f.result() for f in future])

        for nam in self._model.names():
            self._posteriors[nam].fillWith(0)
            for i in range(trajectories):
                self._posteriors[nam] += posteriorsList[i][nam]
            self._posteriors[nam].normalize()

    def posterior(self, name: str) -> gum.Potential:
        p = gum.Potential(self._posteriors[name])
        p._model = self._model
        return p
