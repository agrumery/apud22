import time

import matplotlib.pyplot as plt
import pyAgrum as gum
import pyAgrum.lib.image as gimg

from pyctbn.Ctbn import Ctbn
from pyctbn.CtbnInference import SimpleCtbnInference, ForwardSampleCtbnInference

class TimeIt(object):
    def __init__(self, name):
        self.__name = name
        self.__elapsedTime = -1

    def __enter__(self):
        self.__startTime = time.time()
        return self

    def __exit__(self, *exc):
        self.__elapsedTime = time.time() - self.__startTime
        print('[{}] finished in {} ms'.format(self.__name, int(self.__elapsedTime * 1000)))

    def duration(self):
        return self.__elapsedTime


def exampleMonoVariable():
    ctbn = Ctbn()
    ctbn.add(gum.LabelizedVariable("A", "A", ["a0", "a1"]))

    ctbn.CIM("A")[:] = [[-1, 1], [2, -2]]

    ie = SimpleCtbnInference(ctbn)
    ie.makeInference()
    print(ie.posterior("A"))

    ie = ForwardSampleCtbnInference(ctbn)
    ie.makeInference()
    print(ie.posterior("A"))


def example234():
    ctbn = Ctbn()
    ctbn.add(gum.LabelizedVariable("A", "A", ["a0", "a1"]))
    ctbn.add(gum.LabelizedVariable("B", "B", ["b0", "b1"]))
    ctbn.add(gum.LabelizedVariable("X", "X", ["x0", "x1"]))
    ctbn.add(gum.LabelizedVariable("Y", "Y", ["y0", "y1"]))

    ctbn.addArc("A", "X")
    ctbn.addArc("Y", "X")

    ctbn.addArc("B", "Y")
    ctbn.addArc("X", "Y")

    ctbn.CIM("X")[{"A": "a0", "Y": "y0"}] = [[-1, 1], [2, -2]]
    ctbn.CIM("X")[{"A": "a1", "Y": "y0"}] = [[-10, 10], [20, -20]]

    ctbn.CIM("X")[{"A": "a0", "Y": "y1"}] = [[-5, 5], [6, -6]]
    ctbn.CIM("X")[{"A": "a1", "Y": "y1"}] = [[-50, 50], [60, -60]]

    ctbn.CIM("Y")[{"B": "b0", "X": "x0"}] = [[-30, 30], [-40, 40]]
    ctbn.CIM("Y")[{"B": "b1", "X": "x0"}] = [[-3, 3], [4, -4]]

    ctbn.CIM("Y")[{"B": "b0", "X": "x1"}] = [[-70, 70], [80, -80]]
    ctbn.CIM("Y")[{"B": "b1", "X": "x1"}] = [[-7, 7], [8, -8]]

    plt.imshow(gimg.export(ctbn))
    plt.show()

    q = ctbn.CIM("X").amalgamate(ctbn.CIM("Y"))
    print(q.to_matrix({'A': 0, 'B': 1}))


def exemple():
    ctbn = Ctbn()

    ctbn.add(gum.LabelizedVariable("A", "A", ["a", "b"]))
    ctbn.add(gum.LabelizedVariable("B", "B", ["x", "y"]))

    ctbn.addArc("A", "B")

    ctbn.CIM("A")[:] = [[-5, 5],
                        [4, -4]]

    ctbn.CIM("B")[{"A": "a"}] = [[-5, 5],
                                 [4, -4]]
    ctbn.CIM("B")[{"A": "b"}] = [[-1, 1],
                                 [1, -1]]

    with TimeIt("Simple Inference"):
        ie = SimpleCtbnInference(ctbn)
        ie.makeInference()
        print(ie.posterior("A"))
        print(ie.posterior("B"))

    with TimeIt("Sampling Inference"):
        ie = ForwardSampleCtbnInference(ctbn)
        print(ie.makeInference(timeHorizon=5000))
        print(ie.posterior("A"))
        print(ie.posterior("B"))

    # with TimeIt("Parallelized sampling"):
    #  ie.makeParallelInference(trajectories=10,timeHorizon=5000)
    #  print(ie.posterior("A"))
    #  print(ie.posterior("B"))


def testBN():
    bn = gum.fastBN("A->B")


if __name__ == '__main__':
    print("CTBN V0.1")
    example234()
    exemple()
    exampleMonoVariable()
